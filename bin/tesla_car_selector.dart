void main(List<String> arguments) {
  FalconJet falJet = FalconJet();
  falJet.startEngine();
  falJet.fly();
  falJet.landing();
  falJet.stopEngine();
  falJet.showInfo();
  TeslaModelS modelS = TeslaModelS();
  modelS.showCar();
  modelS.stopEngine();
  modelS.startEngine();
  modelS.stopEngine();
}

abstract class Vehicle {
  late String type;
  late String modelName;
  late String motor;
  late int range;
  late double accelerateTime;
  late int topSpeed;
  late int peakPower;
  bool isEngineStart = false;

  get getType => type;

  set setType(type) => this.type = type;

  get getModelName => modelName;

  set setModelName(modelName) => this.modelName = modelName;

  get getMotor => motor;

  set setMotor(motor) => this.motor = motor;

  get getRange => range;

  set setRange(range) => this.range = range;

  get getAccelerateTime => accelerateTime;

  set setAccelerateTime(accelerateTime) => this.accelerateTime = accelerateTime;

  get getTopSpeed => topSpeed;

  set setTopSpeed(topSpeed) => this.topSpeed = topSpeed;

  get getPeakPower => peakPower;

  set setPeakPower(peakPower) => this.peakPower = peakPower;

  Vehicle(this.type, this.modelName, this.motor, this.range,
      this.accelerateTime, this.topSpeed, this.peakPower);

  startEngine() {
    print("Engine started.");
  }

  stopEngine() {
    print("Engine stopped.");
  }
}

class TeslaCar extends Vehicle with Seat {
  String paint;
  String wheels;
  String interior;
  bool isAutopilot;
  bool isFsd; //Full Self-Driving Capability.
  get getPaint => paint;

  set setPaint(String paint) => this.paint = paint;

  get getWheels => wheels;

  set setWheels(wheels) => this.wheels = wheels;

  get getInterior => interior;

  set setInterior(interior) => this.interior = interior;

  TeslaCar(
      String type,
      String modelName,
      String motor,
      int range,
      double accelerateTime,
      int topSpeed,
      int peakPower,
      this.paint,
      this.wheels,
      this.interior,
      this.isAutopilot,
      this.isFsd)
      : super(
            type, modelName, motor, range, accelerateTime, topSpeed, peakPower);

  @override
  startEngine() {
    if (!isEngineStart) {
      isEngineStart = true;
      print("Tesla car model $modelName engine started.");
    } else {
      print("Engine has started.");
    }
  }

  @override
  stopEngine() {
    if (isEngineStart) {
      isEngineStart = false;
      print("Tesla car model $modelName engine stopped.");
    } else {
      print("Engine has not start yet.");
    }
  }

  showCar() {
    showStat();
    showDetail();
  }

  void showStat() {
    printLine();
    print("           Tesla $modelName");
    print("Range(EPA est.) : $range mi");
    print("0-60 mph*       : $accelerateTime s");
    print("Top Speed       : $topSpeed mph");
    print("Peak Power      : $peakPower hp");
  }

  void showDetail() {
    print("-------------Car Info--------------");
    print("Motor           : $motor");
    print("Paint           : $paint");
    print("Wheels          : $wheels");
    print("Interior        : $interior");
    if (isAutopilot) {
      print("Autopilot system.");
    }
    if (isFsd) {
      print("Full Self-Driving Capability.");
    }
    printLine();
  }

  void printLine() {
    print("-----------------------------------");
  }
}

class TeslaModelS implements TeslaCar {
  @override
  double accelerateTime = 1.99;

  @override
  String interior = "Black and White";

  @override
  bool isAutopilot = true;

  @override
  bool isEngineStart = false;

  @override
  bool isFsd = true;

  @override
  String modelName = "ModelS";

  @override
  String motor = "Tri Motor All-Wheel Drive";

  @override
  String paint = "Midnight Silver Metallic";

  @override
  int peakPower = 1020;

  @override
  int range = 396;

  @override
  int topSpeed = 200;

  @override
  String type = "Car";

  @override
  String wheels = "21\" Arachnid Wheels";

  @override
  get getInterior => interior;

  @override
  get getModelName => modelName;

  @override
  get getMotor => motor;

  @override
  get getPaint => paint;

  @override
  get getPeakPower => peakPower;

  @override
  get getRange => range;

  @override
  get getTopSpeed => topSpeed;

  @override
  get getType => type;

  @override
  get getWheels => wheels;

  @override
  void printLine() {
    print("-----------------------------------");
  }

  @override
  set setAccelerateTime(accelerateTime) {
    this.accelerateTime = accelerateTime;
  }

  @override
  set setInterior(interior) {
    this.interior = interior;
  }

  @override
  set setModelName(modelName) {
    this.modelName = modelName;
  }

  @override
  set setMotor(motor) {
    this.motor = motor;
  }

  @override
  set setPaint(String paint) {
    this.paint = paint;
  }

  @override
  set setPeakPower(peakPower) {
    this.peakPower = peakPower;
  }

  @override
  set setRange(range) {
    this.range = range;
  }

  @override
  set setTopSpeed(topSpeed) {
    this.topSpeed = topSpeed;
  }

  @override
  set setType(type) {
    this.type = type;
  }

  @override
  set setWheels(wheels) {
    this.wheels = wheels;
  }

  @override
  showCar() {
    showStat();
    showDetail();
  }

  @override
  void showDetail() {
    print("-------------Car Info--------------");
    print("Motor           : $motor");
    print("Paint           : $paint");
    print("Wheels          : $wheels");
    print("Interior        : $interior");
    if (isAutopilot) {
      print("Autopilot system.");
    }
    if (isFsd) {
      print("Full Self-Driving Capability.");
    }
    printLine();
  }

  @override
  void showStat() {
    printLine();
    print("           Tesla $modelName");
    print("Range(EPA est.) : $range mi");
    print("0-60 mph*       : $accelerateTime s");
    print("Top Speed       : $topSpeed mph");
    print("Peak Power      : $peakPower hp");
  }

  @override
  startEngine() {
    if (!isEngineStart) {
      isEngineStart = true;
      print("Tesla car model $modelName engine started.");
    } else {
      print("Engine has started.");
    }
  }

  @override
  stopEngine() {
    if (isEngineStart) {
      isEngineStart = false;
      print("Tesla car model $modelName engine stopped.");
    } else {
      print("Engine has not start yet.");
    }
  }

  @override
  get getAccelerateTime => accelerateTime;

  @override
  int seatNumber = 2;

  @override
  get getSeatNumber => seatNumber;

  @override
  set setSeatNumber(int number) {
    this.seatNumber = seatNumber;
  }
}

mixin Seat {
  int seatNumber = 4;

  set setSeatNumber(int number) => seatNumber = number;

  get getSeatNumber => seatNumber;
}

class flyable {
  void fly() {}
  void landing() {}
}

abstract class AirCraft extends Vehicle implements flyable {
  bool isFlying = false;
  AirCraft(String type, String modelName, String motor, int range,
      double accelerateTime, int topSpeed, int peakPower)
      : super(
            type, modelName, motor, range, accelerateTime, topSpeed, peakPower);
}

abstract class RotorCraft extends Vehicle {
  RotorCraft(String type, String modelName, String motor, int range,
      double accelerateTime, int topSpeed, int peakPower)
      : super(
            type, modelName, motor, range, accelerateTime, topSpeed, peakPower);
}

class Airplane extends AirCraft with Seat {
  String cabinLength;
  String cabinWidth;
  String cabinHeight;
  String cabinVolume;
  String internalBaggage;
  String externalBaggage;

  Airplane(
      String type,
      String modelName,
      String motor,
      int range,
      double accelerateTime,
      int topSpeed,
      int peakPower,
      this.cabinLength,
      this.cabinWidth,
      this.cabinHeight,
      this.cabinVolume,
      this.internalBaggage,
      this.externalBaggage)
      : super(
            type, modelName, motor, range, accelerateTime, topSpeed, peakPower);

  void showInfo() {}

  void printLine() {}

  void showStat() {}

  void showDetail() {}

  @override
  void fly() {
    if (isEngineStart) {
      isFlying = true;
      print("Jet is fly");
    } else {
      print("Please start engine before fly.");
    }
  }

  @override
  void landing() {
    if (isFlying) {
      isFlying = false;
      print("Jet is landed");
    } else {
      print("Jet is not flying.");
    }
  }
}

class Jet extends Airplane {
  Jet(
      String type,
      String modelName,
      String motor,
      int range,
      double accelerateTime,
      int topSpeed,
      int peakPower,
      String cabinLength,
      String cabinWidth,
      String cabinHeight,
      String cabinVolume,
      String internalBaggage,
      String externalBaggage)
      : super(
            type,
            modelName,
            motor,
            range,
            accelerateTime,
            topSpeed,
            peakPower,
            cabinLength,
            cabinWidth,
            cabinHeight,
            cabinVolume,
            internalBaggage,
            externalBaggage);
}

class FalconJet implements Jet {
  @override
  double accelerateTime = 1.5;

  @override
  String cabinHeight = "6' 2\"";

  @override
  String cabinLength = "31'";

  @override
  String cabinVolume = "1024";

  @override
  String cabinWidth = "7' 8\"";

  @override
  String externalBaggage = "131";

  @override
  String internalBaggage = "0";

  @override
  String modelName = "Falcon 2000";

  @override
  String motor = "Fly private";

  @override
  int peakPower = 3000;

  @override
  int range = 3188;

  @override
  int seatNumber = 10;

  @override
  int topSpeed = 700;

  @override
  String type = "Heavy Jet";

  @override
  get getAccelerateTime => accelerateTime;

  @override
  get getModelName => modelName;

  @override
  get getMotor => motor;

  @override
  get getPeakPower => peakPower;

  @override
  get getRange => range;

  @override
  get getSeatNumber => seatNumber;

  @override
  get getTopSpeed => topSpeed;

  @override
  get getType => type;

  @override
  void printLine() {
    print("-----------------------------------");
  }

  @override
  set setAccelerateTime(accelerateTime) {
    this.accelerateTime = accelerateTime;
  }

  @override
  set setModelName(modelName) {
    this.modelName = modelName;
  }

  @override
  set setMotor(motor) {
    this.motor = motor;
  }

  @override
  set setPeakPower(peakPower) {
    this.peakPower = peakPower;
  }

  @override
  set setRange(range) {
    this.range = range;
  }

  @override
  set setSeatNumber(int number) {
    this.seatNumber = number;
  }

  @override
  set setTopSpeed(topSpeed) {
    this.topSpeed = topSpeed;
  }

  @override
  set setType(type) {
    this.type = type;
  }

  @override
  void showDetail() {
    print("-------------Detail----------------");
    print("         Jet $modelName");
    print("Cabin Length    : $cabinLength");
    print("Cabin Width     : $cabinWidth");
    print("Cabin Height    : $cabinHeight");
    print("Cabin Volume    : $cabinVolume");
    print("Internal Baggage: $internalBaggage");
    print("External Baggage: $externalBaggage");
    printLine();
  }

  @override
  void showInfo() {
    showStat();
    showDetail();
  }

  @override
  void showStat() {
    printLine();
    print("         Jet $modelName");
    print("Range(EPA est.) : $range mi");
    print("0-120 mph*      : $accelerateTime s");
    print("Top Speed       : $topSpeed mph");
    print("Peak Power      : $peakPower hp");
  }

  @override
  startEngine() {
    isEngineStart = true;
    print("Jet engine started.");
  }

  @override
  stopEngine() {
    if (isEngineStart) {
      if (!isFlying) {
        isEngineStart = false;
        print("Jet engine Stopped.");
      } else {
        print("Please landing before stop the engine.");
      }
    } else {
      print("Engine is not started.");
    }
  }

  @override
  void fly() {
    if (isEngineStart) {
      isFlying = true;
      print("Jet is fly");
    } else {
      print("Please start engine before fly.");
    }
  }

  @override
  void landing() {
    if (isFlying) {
      isFlying = false;
      print("Jet is landed");
    } else {
      print("Jet is not flying.");
    }
  }

  @override
  bool isEngineStart = false;

  @override
  bool isFlying = false;
}
